<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/AlmacenRegistro.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $obj = new AlmacenRegistro;
    $resultado = $obj->historial_RegistroCaja();

    $listadepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_registro" => $resultado[$i]["id_registro"],
            "id_historial" => $resultado[$i]["id_historial"],
            "cantidad" => $resultado[$i]["cantidad"],
            "fecha" => $resultado[$i]["fecha"],
            "operacion" => $resultado[$i]["operacion"],
            "hora" => $resultado[$i]["hora"],
            "nombre_cliente" => $resultado[$i]["nombre_cliente"],
            "nombre_caja" => $resultado[$i]["nombre_caja"],
            "nombre_ubicacion" => $resultado[$i]["nombre_ubicacion"],
            "sub_total" => $resultado[$i]["sub_total"]
        );

        $listadepartamento[$i] = $datos;
    }

    Funciones::imprimeJSON(200, "", $listadepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}