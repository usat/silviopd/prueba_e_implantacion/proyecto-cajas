<?php

require_once dirname(__FILE__).'/../datos/Conexion.clase.php';

class AlmacenRegistro extends Conexion {

    private $id_registro, $contacto, $cantidad, $observaciones, $fecha_registro_caja, $id_ubicacion, $id_cliente, $operacion, $stock, $id_caja, $id_usuario_area;

    function getId_registro() {
        return $this->id_registro;
    }

    function getContacto() {
        return $this->contacto;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getObservaciones() {
        return $this->observaciones;
    }

    function getFecha_registro_caja() {
        return $this->fecha_registro_caja;
    }

    function getId_ubicacion() {
        return $this->id_ubicacion;
    }

    function getOperacion() {
        return $this->operacion;
    }

    function getStock() {
        return $this->stock;
    }

    function getId_caja() {
        return $this->id_caja;
    }

    function getId_usuario_area() {
        return $this->id_usuario_area;
    }

    function setId_registro($id_registro) {
        $this->id_registro = $id_registro;
    }

    function setContacto($contacto) {
        $this->contacto = $contacto;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;
    }

    function setFecha_registro_caja($fecha_registro_caja) {
        $this->fecha_registro_caja = $fecha_registro_caja;
    }

    function setId_ubicacion($id_ubicacion) {
        $this->id_ubicacion = $id_ubicacion;
    }

    function setOperacion($operacion) {
        $this->operacion = $operacion;
    }

    function setStock($stock) {
        $this->stock = $stock;
    }

    function setId_caja($id_caja) {
        $this->id_caja = $id_caja;
    }

    function setId_usuario_area($id_usuario_area) {
        $this->id_usuario_area = $id_usuario_area;
    }

    function getId_cliente() {
        return $this->id_cliente;
    }

    function setId_cliente($id_cliente) {
        $this->id_cliente = $id_cliente;
    }

    public function leerDatos() {
        try {
            $sql = "SELECT rc.id_registro, rc.contacto, rc.cantidad, rc.observaciones, rc.fecha_registro_caja, rc.operacion, rc.id_caja, rc.id_usuario_area, rc.stock_anterior, rc.stock_actual, rc.sub_total, rc.id_ubicacion, rc.id_cliente, u.nombre as nombre_ubicacion, u.capacidad
                    FROM registro_caja rc
                            INNER JOIN ubicacion u ON ( rc.id_ubicacion = u.id_ubicacion  )
                            where rc.id_registro = :p_id_registro";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_registro", $this->getId_registro());
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listar() {
        try {
            $sql = "SELECT
                    rc.id_registro,
                    rc.contacto,
                    rc.cantidad,
                    rc.observaciones,
                    DATE_FORMAT(rc.fecha_registro_caja,'%d-%m-%Y') as fecha,
                    DATE_FORMAT(rc.fecha_registro_caja,'%l:%i %p') as hora,
                    rc.operacion,
                    rc.id_caja,
                    c.nombre as nombre_caja,
                    rc.stock_anterior,
                    rc.stock_actual,
                    rc.id_ubicacion,
                    u.nombre as nombre_ubicacion,
                    u.capacidad,
                    rc.id_cliente,
                    c1.nombre as nombre_cliente,
                    DATEDIFF( NOW(),DATE(rc.fecha_registro_caja)) dias_transcurridos,
                    rc.id_usuario_area,
                    concat(c2.nombres,' ',c2.apellidos) as nombre_usuario
                  FROM registro_caja rc
                          INNER JOIN caja c ON ( rc.id_caja = c.id_caja  )
                          INNER JOIN usuario_area ua ON ( rc.id_usuario_area = ua.id_usuario_area  )
                                  INNER JOIN usuario u1 ON ( ua.id_usuario = u1.id_usuario  )
                                          INNER JOIN colaborador c2 ON ( u1.id_usuario = c2.dni  )
                          INNER JOIN ubicacion u ON ( rc.id_ubicacion = u.id_ubicacion  )
                          INNER JOIN cliente c1 ON ( rc.id_cliente = c1.id_cliente  )
                    WHERE rc.stock_actual > 0
                    ORDER BY 1 DESC";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function agregar() {
        $this->dblink->beginTransaction();

        try {

            $sql = "CALL  f_registro_caja( :p_contacto, :p_cantidad, :p_observaciones, :p_operacion,:p_id_caja, :p_id_usuario_area, :p_id_ubicacion, :p_id_cliente);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_contacto", $this->getContacto());
            $sentencia->bindValue(":p_cantidad", $this->getCantidad());
            $sentencia->bindValue(":p_observaciones", $this->getObservaciones());
            $sentencia->bindValue(":p_operacion", $this->getOperacion());
            $sentencia->bindValue(":p_id_caja", $this->getId_caja());
            $sentencia->bindValue(":p_id_usuario_area", $this->getId_usuario_area());
            $sentencia->bindValue(":p_id_ubicacion", $this->getId_ubicacion());
            $sentencia->bindValue(":p_id_cliente", $this->getId_cliente());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacci贸n
            throw $exc;
        }

        return false;
    }

    public function ingresar_retirar() {
        $this->dblink->beginTransaction();

        try {

            $sql = "CALL  f_registro_caja_ingreso_salida( :p_id_registro, :p_contacto, :p_cantidad, :p_observaciones, :p_operacion, :p_id_usuario_area);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_registro", $this->getId_registro());
            $sentencia->bindValue(":p_contacto", $this->getContacto());
            $sentencia->bindValue(":p_cantidad", $this->getCantidad());
            $sentencia->bindValue(":p_observaciones", $this->getObservaciones());
            $sentencia->bindValue(":p_operacion", $this->getOperacion());
            $sentencia->bindValue(":p_id_usuario_area", $this->getId_usuario_area());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacci贸n
            throw $exc;
        }

        return false;
    }

    public function buscarStock() {
        try {

            $sql = "select stock from caja where id_caja = :p_id_caja";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_caja", $this->getId_caja());
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            $this->dblink->rollBack();
            throw $exc;
        }
    }

    /* FUNCION DEL REPORTE */

    public function reporteRegistro() {
        try {
            $sql = "select *,DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                            DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) = CURDATE() AND
                            lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                            and id_caja = 11";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function reporteRegistroCajones() {
        try {
            $sql = "select *,DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                            DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) = CURDATE() AND
                            lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                            and id_caja = 1";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function reporteRegistroSillas() {
        try {
            $sql = "select *,DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                            DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) = CURDATE() AND
                            lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                            and id_caja = 13";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function reporteRegistroConteoEfectivo() {
        try {
            $sql = "select *,DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                            DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) = CURDATE() AND
                            lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                            and id_caja = 14";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function reporteRegistroCajasNuevas() {
        try {
            $sql = "select *,DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                            DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) = CURDATE() AND
                            lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                            and id_caja = 12";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function historial_RegistroCaja() {
        try {
            $sql = "select
                            hrc.id_registro,
                            hrc.id_historial,
                            hrc.cantidad,
                            case when hrc.operacion = 'I' then 'INGRESO' else 'SALIDA' end as operacion,
                            date_format(hrc.fecha_registro_caja,'%d-%m-%Y') AS fecha,
                            date_format(hrc.fecha_registro_caja,'%l:%i %p') AS hora,
                            hrc.sub_total,
                        ci.nombre as nombre_cliente,
                        ca.nombre as nombre_caja,
                        u.nombre as nombre_ubicacion
                    from historial_registro_caja hrc inner join registro_caja rc on hrc.id_registro=rc.id_registro
                            inner join cliente ci on ci.id_cliente = rc.id_cliente
                        inner join caja ca on rc.id_caja = ca.id_caja
                        inner join ubicacion u on u.id_ubicacion = rc.id_ubicacion
                    where  hrc.estado_det='H'
                    order by 5";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function anularRegistro($p_tabla, $id_user, $p_id, $p_nro_placa, $p_op) {
        $this->dblink->beginTransaction();

        try {

            $sql = "call fn_anular(:p_tabla,:p_id_user,:p_id,:p_nro_placa,:p_op);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_tabla", $p_tabla);
            $sentencia->bindValue(":p_id_user", $id_user);
            $sentencia->bindValue(":p_id", $p_id);
            $sentencia->bindValue(":p_nro_placa", $p_nro_placa);
            $sentencia->bindValue(":p_op", $p_op);
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacci贸n
            throw $exc;
        }

        return false;
    }

    public function listarReporteRegistroFechas($fecha_inicio, $fecha_final) {
        try {
            $sql = "select *,
                        DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                        DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) >= :p_fecha_inicio and date(fecha_registro_caja) <= :p_fecha_final AND
                        lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                        and id_caja = 11";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_fecha_inicio", $fecha_inicio);
            $sentencia->bindValue(":p_fecha_final", $fecha_final);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function listarReporteRegistroFechasCajasNuevas($fecha_inicio, $fecha_final) {
        try {
            $sql = "select *,
                        DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                        DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) >= :p_fecha_inicio and date(fecha_registro_caja) <= :p_fecha_final AND
                        lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                        and id_caja = 12";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_fecha_inicio", $fecha_inicio);
            $sentencia->bindValue(":p_fecha_final", $fecha_final);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function listarReporteRegistroFechasCajones($fecha_inicio, $fecha_final) {
        try {
            $sql = "select *,
                        DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                        DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) >= :p_fecha_inicio and date(fecha_registro_caja) <= :p_fecha_final AND
                        lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                        and id_caja = 1";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_fecha_inicio", $fecha_inicio);
            $sentencia->bindValue(":p_fecha_final", $fecha_final);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function listarReporteRegistroFechasSillas($fecha_inicio, $fecha_final) {
        try {
            $sql = "select *,
                        DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                        DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) >= :p_fecha_inicio and date(fecha_registro_caja) <= :p_fecha_final AND
                        lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                        and id_caja = 13";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_fecha_inicio", $fecha_inicio);
            $sentencia->bindValue(":p_fecha_final", $fecha_final);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function listarReporteRegistroFechasConteoEfectivo($fecha_inicio, $fecha_final) {
        try {
            $sql = "select *,
                        DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                        DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
                    from v_reporte_almacen_caja
                    WHERE date(fecha_registro_caja) >= :p_fecha_inicio and date(fecha_registro_caja) <= :p_fecha_final AND
                        lower(nombre_cliente) != lower('ecomphisa') AND operacion = 'SALIDA'
                        and id_caja = 14";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_fecha_inicio", $fecha_inicio);
            $sentencia->bindValue(":p_fecha_final", $fecha_final);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function reporteRegistroEcomphisa() {
        try {
            $sql = "SELECT  hrc.id_registro,
                    hrc.id_historial,
                    rc.id_cliente,
                    c1.nombre AS nombre_cliente,
                    hrc.contacto,
                    hrc.cantidad,
                    hrc.observaciones,
                    hrc.fecha_registro_caja,
                    DATE_FORMAT(hrc.fecha_registro_caja,'%d-%m-%Y') as fecha,
                    DATE_FORMAT(hrc.fecha_registro_caja,'%l:%i %p') as hora,
                    (CASE WHEN hrc.operacion = 'I' then 'INGRESO' ELSE 'SALIDA' END) AS operacion,
                    hrc.id_usuario_area,
                    hrc.stock_anterior,
                    hrc.stock_actual,
                    hrc.sub_total,
                    rc.id_caja,
                    c.precio_base,
                    c.nombre as nombre_caja,
                    rc.id_ubicacion,
                    u.nombre AS nombre_ubicacion,
                    concat(c2.nombres,' ',c2.apellidos) AS nombre_usuario,
                    DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                    DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
            FROM historial_registro_caja hrc
                    INNER JOIN registro_caja rc ON ( hrc.id_registro = rc.id_registro  )
                            INNER JOIN caja c ON ( rc.id_caja = c.id_caja  )
                            INNER JOIN usuario_area ua ON ( rc.id_usuario_area = ua.id_usuario_area  )
                                    INNER JOIN usuario u1 ON ( ua.id_usuario = u1.id_usuario  )
                                            INNER JOIN colaborador c2 ON ( u1.id_usuario = c2.dni  )
                            INNER JOIN ubicacion u ON ( rc.id_ubicacion = u.id_ubicacion  )
                            INNER JOIN cliente c1 ON ( rc.id_cliente = c1.id_cliente  )
            WHERE date(hrc.fecha_registro_caja) = CURDATE() AND
                 lower(c1.nombre) = lower('ecomphisa') AND hrc.operacion = 'S'
                 and hrc.estado_det='H'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listarReporteRegistroEcomphisaFechas($fecha_inicio, $fecha_final) {
        try {
            $sql = "SELECT  hrc.id_registro,
                    hrc.id_historial,
                    rc.id_cliente,
                    c1.nombre AS nombre_cliente,
                    hrc.contacto,
                    hrc.cantidad,
                    hrc.observaciones,
                    hrc.fecha_registro_caja,
                    DATE_FORMAT(hrc.fecha_registro_caja,'%d-%m-%Y') as fecha,
                    DATE_FORMAT(hrc.fecha_registro_caja,'%l:%i %p') as hora,
                    (CASE WHEN hrc.operacion = 'I' then 'INGRESO' ELSE 'SALIDA' END) AS operacion,
                    hrc.id_usuario_area,
                    hrc.stock_anterior,
                    hrc.stock_actual,
                    hrc.sub_total,
                    rc.id_caja,
                    c.precio_base,
                    c.nombre as nombre_caja,
                    rc.id_ubicacion,
                    u.nombre AS nombre_ubicacion,
                    concat(c2.nombres,' ',c2.apellidos) AS nombre_usuario,
                    DATE_FORMAT(NOW(),'%d-%m-%Y') as fecha_actual,
                    DATE_FORMAT(NOW(),'%l:%i %p') as hora_actual
            FROM historial_registro_caja hrc
                    INNER JOIN registro_caja rc ON ( hrc.id_registro = rc.id_registro  )
                            INNER JOIN caja c ON ( rc.id_caja = c.id_caja  )
                            INNER JOIN usuario_area ua ON ( rc.id_usuario_area = ua.id_usuario_area  )
                                    INNER JOIN usuario u1 ON ( ua.id_usuario = u1.id_usuario  )
                                            INNER JOIN colaborador c2 ON ( u1.id_usuario = c2.dni  )
                            INNER JOIN ubicacion u ON ( rc.id_ubicacion = u.id_ubicacion  )
                            INNER JOIN cliente c1 ON ( rc.id_cliente = c1.id_cliente  )
            WHERE date(hrc.fecha_registro_caja) >= :p_fecha_inicio and date(hrc.fecha_registro_caja) <= :p_fecha_final AND
                 lower(c1.nombre) = lower('ecomphisa') AND hrc.operacion = 'S'
                 and hrc.estado_det='H'";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_fecha_inicio", $fecha_inicio);
            $sentencia->bindValue(":p_fecha_final", $fecha_final);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function correlativo() {
        try {
            $sql = "select count(*) as correlativo from historial_registro_caja";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function ticket() {
        try {
            $sql = "SELECT  hrc.id_registro,
                            hrc.contacto,
                            hrc.cantidad,
                            hrc.observaciones,
                            DATE_FORMAT(hrc.fecha_registro_caja,'%d-%m-%Y') as fecha,
                            DATE_FORMAT(hrc.fecha_registro_caja,'%l:%i %p') as hora,
                            hrc.operacion,
                            hrc.id_usuario_area,
                            hrc.stock_anterior,
                            hrc.stock_actual,
                            hrc.sub_total,
                            hrc.id_historial,
                            c.nombre as nombre_caja,
                            c.precio_base,
                            concat(c1.nombres,' ',c1.apellidos) as nombre_usuario,
                            u1.nombre as nombre_ubicacion,
                            c2.nombre as nombre_cliente,
                            CASE 
                                WHEN DATEDIFF( NOW(),DATE(rc.fecha_registro_caja)) >= DATEDIFF( DATE(ADDDATE(ADDDATE(rc.fecha_registro_caja, INTERVAL 3 MONTH), INTERVAL 1 DAY) ), DATE(rc.fecha_registro_caja) )  THEN
                                    c.precio_retraso+0.20
                                WHEN DATEDIFF( NOW(),DATE(rc.fecha_registro_caja)) >= DATEDIFF( DATE(ADDDATE(ADDDATE(rc.fecha_registro_caja, INTERVAL 2 MONTH), INTERVAL 1 DAY) ), DATE(rc.fecha_registro_caja) )  THEN
                                    c.precio_retraso+0.20
                                WHEN DATEDIFF( NOW(),DATE(rc.fecha_registro_caja)) >= DATEDIFF( DATE(ADDDATE(ADDDATE(rc.fecha_registro_caja, INTERVAL 1 MONTH), INTERVAL 1 DAY) ), DATE(rc.fecha_registro_caja) )  THEN
                                    c.precio_retraso
                                ELSE
                                    c.precio_base
                            END as precio_final
                    FROM historial_registro_caja hrc
                            INNER JOIN registro_caja rc ON ( hrc.id_registro = rc.id_registro  )
                                    INNER JOIN caja c ON ( rc.id_caja = c.id_caja  )
                                    INNER JOIN usuario_area ua ON ( rc.id_usuario_area = ua.id_usuario_area  )
                                            INNER JOIN usuario u ON ( ua.id_usuario = u.id_usuario  )
                                                    INNER JOIN colaborador c1 ON ( u.id_usuario = c1.dni  )
                                    INNER JOIN ubicacion u1 ON ( rc.id_ubicacion = u1.id_ubicacion  )
                                    INNER JOIN cliente c2 ON ( rc.id_cliente = c2.id_cliente  )
                    WHERE hrc.id_registro = :p_id_registro AND hrc.operacion = 'S'
                    ORDER BY hrc.id_historial DESC LIMIT 1  ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindValue(":p_id_registro", $this->getId_registro());
            $sentencia->execute();
            $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);

            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    //put your code here
}
