<?php

require_once dirname(__FILE__) . '/../../negocio/AlmacenCaja.php';
include dirname(__FILE__) . '/../../simpletest/autorun.php';

class TestAlmacenCaja extends UnitTestCase {

    public function testAgregar() {
        $obj = new AlmacenCaja();
        strtoupper($obj->setNombre("TEST2"));
        $obj->setPrecio_base("20.00");
        $obj->setPrecio_retraso("30.00");
        $obj->setEstado("H");
        $this->assertTrue($obj->agregar());
    }
}
