# SISTEMA WEB DE GESTIÓN DE ALMACÉN  
&nbsp;  

### INTEGRANTES 
* Diaz Anchay, Oscar Fernando  
* Peña Diaz, Silvio Reynaldo 

### NOMBRE DEL SISTEMA
* SISTEMA WEB DE GESTIÓN DE ALMACÉN  

### DELIMITACIÓN DEL SISTEMA
* El sistema esta basado en el control de gestion de las cajas en almacen

### REQUERIMIENTOS FUNCIONALES   
* El sistema contará con un inicio de sesión, previamente registrado en caso contrario se tendrá que hablar con el administrador para crear las credenciales.
* El sistema contará con la posibilidad de hacer seguimiento de la ubicacion de las cajas registradas.
* Se permitirá el registro y edicion de cliente, asi como la posibilidad de darle de baja (Deshabilitado)  
* El sistema contará con el registro y edicion del nombre de la caja, precio base y precio retraso; tomando como restriccion si la caja no es retirada durante 1 mes se le cargará el precio retraso mas el precio base; si la caja no es retirado durante 2 meses o más se le cargará 2 veces el precio retraso más el precio base 
* El sistema contará con el registro de transacciones que permitirá la suma o resta de cajas al cliente, una vez llegada a 0 las cajas (stock actual) en almacén no se listará y se deberá crear un nuevo registro del número de cajas en almacen a dicho cliente
* El sistema cuenta con un reporte de transacciones totales de las cajas que no pertenencen a la empresa ecomphisa (Reporte De Cajas Registro)  
* El sistema cuenta con un reporte de transacciones totales de las cajas que pertenencen a la empresa ecomphisa (Reporte De Cajas Alquiler)
* El sistema contarará con un reporte para las transacciones de cajas nuevas (Reporte De cajas Nuevas)
* El sistema contarará con un reporte para las transacciones de cajones (Reporte De Cajones)
* El sistema contarará con un reporte para las transacciones de sillas (Reporte De Sillas)
* El sistema contarará con un reporte para las transacciones de conteo (Reporte De Conteo)

### REQUERIMIENTOS NO FUNCIONALES
#### De Usabilidad
* El sistema contará con manuales instructivos del uso del sistema
* El sistema cuenta con un diseño WebResponsive para que el cliente pueda realizar sus consultas por el móvil celular.
* El sistema cuenta con una interfaz amigable para el usuario.
* El sistema muestra los errores informativos amigables para el usuario final.
* El tiempo de atención para las ventas se reduce.  

#### De Eficiencia
* Toda transacción realizada por el usuario debe responder rápida.
* El sistema debe ser capaz de tolerar multiples realizando transacciones.
* Los datos ingresados al sistema deben ser actualizados.  

#### De Seguridad
* Para el ingreso al sistema se tendrá en cuenta tener credenciales de acceso para cada usuario
* El manejo de las transacciones se estará monitoreando en la base de datos, ya que se registrará el usuario que la realizo.
* Si se identifica un fallo o brecha del sistema, el usuario no seguirá operando a menos que el administrador del sistema lo autorice, luego de haber realizado la corrección.  

#### El sistema cuenta con 3 partes:  

* Back-end: Web Services hecha en el lenguaje php  
* Front-end: hecha en html,css3 y javascript
* DataBase: hecha en mysql 

### DATOS DE ACCESO
* Pagina: <a href="http://proyecto-cajas.herokuapp.com/" target="_blank">INGRESAR AQUI</a>  
* **Username:** 46705014  
* **Password:** CajaMarca2018

### MANUAL DE USUARIO
* Pagina: <a href="http://ws-proyecto-cajas.wilderguevara.com/manuales/MODULO3.pdf" target="_blank">INGRESAR AQUI</a>

